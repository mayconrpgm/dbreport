# DBReport
# Written in MySQL Workbench

from wb import *
import mforms
import os
from mforms import FileChooser

ModuleInfo = DefineModule(name="DBReport", author="MT", version="1.0", description="Contains Plugin DbReport")

@ModuleInfo.plugin("DBReport", caption="DB Report", description="DB Report", pluginMenu="Utilities")
@ModuleInfo.export(grt.INT)
def DBReport():
    DirChooser = FileChooser(mforms.OpenDirectory)
    if DirChooser.run_modal():      
        if DirChooser.get_path() != "":
            html = open(DirChooser.get_path() + os.path.sep + "Report.html", "w")
            html.write( "<!DOCTYPE html>\n")
            html.write("<html xmlns=\"http://www.w3.org/1999/xhtml\">\n")
            html.write("<head>\n")
            html.write(" <meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\" />\n")
            html.write(" <title>DB Report</title>\n")
            html.write(" <style type=\"text/css\">")
            html.write("    a:link {\n")
            html.write("        text-decoration:none;\n")
            html.write("        color:#666699;\n")
            html.write("    }\n")
            html.write("    a:visited {\n")
            html.write("        text-decoration:none;\n")
            html.write("        color:#666699;\n")
            html.write("    }\n")
            html.write("    a:hover {\n")
            html.write("        text-decoration:underline;\n")
            html.write("        color:#666699;\n")
            html.write("    }\n")
            html.write("    a:active {\n")
            html.write("        text-decoration:underline;\n")
            html.write("        color:#666699;\n")
            html.write("    }\n")
            html.write("    body {\n")
            html.write("    	font-family: \"Lucida Sans Unicode\", \"Lucida Grande\", Sans-Serif;\n")
            html.write("    	font-size : 12px ;\n")
            html.write("    	margin : 0px ;\n")
            html.write("    	display : block ;\n")
            html.write("    }\n\n")
            html.write("    table {\n")
            html.write("	    background: #FFFFFF;\n")
            html.write("	    width: 100%;\n")
            html.write("    	border-collapse: collapse;\n")
            html.write("    	text-align: left;\n")
            html.write("        table-layout: fixed;")
            html.write("    }\n\n")
            html.write("    table th {\n")
            html.write("    	font-size: 14px;\n")
            html.write("    	font-weight: normal;\n")
            html.write("    	color: #039;\n")
            html.write("    	padding: 10px;\n")
            html.write("    	text-align: left;\n")
            html.write("    	border-bottom: 2px solid #6678b1;\n")
            html.write("    }\n\n")
            html.write("    table td {\n")
            html.write("    	border-bottom: 1px solid #CCCCFF;\n")
            html.write("    	font-size: 12px;\n")
            html.write("    	color: #666699;\n")
            html.write("    	padding: 5px;\n")
            html.write("    	vertical-align:top;\n")
            html.write("    	word-wrap: break-word;\n")
            html.write("    }\n\n")
            html.write("    table tbody tr:hover td {\n")
            html.write("    	color: #000099;\n")
            html.write("    }\n\n")
            html.write("    .subtitle {\n")
            html.write("    	color: #6678B1;\n")
            html.write("    	font-weight : bold;\n")
            html.write("    	padding : 10px;\n")
            html.write("    }\n\n")
            html.write("    .title {\n")
            html.write("    	background: #6678B1;\n")
            html.write("    	color: #FFFFFF;\n")
            html.write("    	border: 1px solid #888888;\n")
            html.write("    	font-size : 14pt;\n")
            html.write("    	font-weight : bold;\n")
            html.write("    	padding : 10px;\n")
            html.write("    	margin: 10px;\n")
            html.write("    }\n\n")
            html.write("    .theader {\n")
            html.write("    	background: #6678B1;\n")
            html.write("    	color: #FFFFFF;\n")
            html.write("    	font-weight : bold;\n")
            html.write("    	border-left: 1px solid #BBBBFF;\n")
            html.write("    	border-top: 1px solid #BBBBFF;\n")
            html.write("    	border-right: 1px solid #BBBBFF;\n")
            html.write("    	font-size : 10pt;\n")
            html.write("    	padding : 10px;\n")
            html.write("    	margin-left : 10px;\n")
            html.write("    	margin-right : 10px;\n")
            html.write("    }\n\n")
            html.write("    .tbody {\n")
            html.write("    	background: #EEEEFF;\n")
            html.write("    	border-left: 1px solid #BBBBFF;\n")
            html.write("    	border-bottom: 1px solid #BBBBFF;\n")
            html.write("    	border-right: 1px solid #BBBBFF;\n")
            html.write("    	padding: 10px;\n")
            html.write("    	margin-left : 10px;\n")
            html.write("    	margin-right : 10px;\n")
            html.write("    	margin-bottom : 10px;\n")
            html.write("    }")
            html.write(" </style>\n")
            html.write("</head>\n")
            html.write("<body>\n")

            html.write(" <div id=\"index\" class=\"title\">Index\n")  
            for schema in grt.root.wb.doc.physicalModels[0].catalog.schemata:
                html.write("         <table>\n")
                html.write("             <thead>\n")
                html.write("                 <tr>\n")
                html.write("                     <th>" + schema.name + "</th>\n")
                html.write("                 </tr>\n")
                html.write("             </thead>\n")
                html.write("             <tbody>\n")
                for table in sorted(schema.tables, key=lambda table: table.name):
                    html.write("                <tr>")
                    html.write("                    <td>")
                    html.write("                        <a HREF=\"#" + schema.name + "." + table.name + "\">" + table.name + "</a>\n")
                    html.write("                    </td>")
                    html.write("                </tr>")
                if len(schema.views) > 0:
                    html.write("                <tr>")
                    html.write("                    <td>")
                    html.write("                        <a HREF=\"#" + schema.name + ".Views\">Views</a>\n")
                    html.write("                    </td>")
                    html.write("                </tr>")
                if len(schema.routines) > 0:
                    html.write("                <tr>")
                    html.write("                    <td>")
                    html.write("                        <a HREF=\"#" + schema.name + ".Routines\">Routines</a>\n")
                    html.write("                    </td>")
                    html.write("                </tr>")
                html.write("             </tbody>\n")
                html.write("         </table>\n")
            html.write(" </div>")

            for schema in grt.root.wb.doc.physicalModels[0].catalog.schemata:
                html.write(" <div class=\"title\">" + schema.name + "</div>\n")        

                for table in sorted(schema.tables, key=lambda table: table.name):
                    html.write(" <div id=\"" + schema.name + "." + table.name + "\" class=\"theader\">" + table.name + " - <a href=\"#index\">Index</a></div>\n")

                    html.write(" <div class=\"tbody\">\n")
                    html.write("     <div class=\"subtitle\">" + table.comment + "</div>\n")
                    html.write("     <div class=\"subtitle\">Columns</div>\n")

                    html.write("         <table>\n")
                    html.write("             <thead>\n")
                    html.write("                 <tr>\n")
                    html.write("                     <th style=\"width:200px\">Column Name</th>\n")
                    html.write("                     <th style=\"width:300px\">Type</th>\n")
                    html.write("                     <th style=\"width:100%\">Comment</th>\n")
                    html.write("                 </tr>\n")
                    html.write("             </thead>\n")
                    html.write("             <tbody>\n")
                
                    for column in table.columns:
                        html.write("                 <tr>\n")
                        html.write("                     <td>" + column.name + "</td>\n")
                        html.write("                     <td>" + column.formattedType)
                        for flag in column.flags:
                            html.write(" " + flag)
                        if column.isNotNull == 1: 
                            html.write(" NOT NULL" )
                        if column.autoIncrement == 1:
                            html.write(" AUTO_INCREMENT")
                        html.write(" " + column.defaultValue)
                        html.write("</td>\n")
                        html.write("                     <td class=\"tdfill\">" + column.comment.replace("\n", "<br />") + "</td>\n" )
                        html.write("                 </tr>\n")

                    html.write("             </tbody>\n")
                    html.write("         </table>\n")

                    html.write("         <div class=\"subtitle\">Keys</div>\n")

                    html.write("         <table>\n")
                    html.write("             <thead>\n")
                    html.write("                 <tr>\n")
                    html.write("                     <th>Key Name</th>\n")
                    html.write("                     <th>Type</th>\n")
                    html.write("                     <th>Columns Name</th>\n")
                    html.write("                 </tr>\n")
                    html.write("             </thead>\n")

                    html.write("             <tbody>\n")

                    for index in table.indices:
                        html.write("                 <tr>\n")
                        html.write("                     <td>" + index.name + "</td>\n")
                        html.write("                     <td>" + index.indexType + "</td>\n" )
                        Col = ""
                        for column in index.columns:
                            if Col != "":
                                Col += ", "
                            Col += column.referencedColumn.name
                        html.write("                     <td>" + Col + "</td>\n" )
                        html.write("             </tr>\n")

                    html.write("             </tbody>\n")
                    html.write("         </table>\n")

                    if len(table.foreignKeys) > 0:
                        html.write("     <div class=\"subtitle\">Foreign Keys</div>\n")

                        html.write("     <table>\n")
                        html.write("         <thead>\n")
                        html.write("             <tr>\n")
                        html.write("                 <th>Foreign Key Name</th>\n")
                        html.write("                 <th>Columns</th>\n")
                        html.write("                 <th>Referenced Table Name</th>\n")
                        html.write("                 <th>Referenced Columns</th>\n")
                        html.write("             </tr>\n")
                        html.write("         </thead>\n")
                        html.write("         <tbody>\n")

                        for key in table.foreignKeys:
                            html.write("             <tr>\n")
                            html.write("                 <td>" + key.name + "</td>\n" )
                            Col = ""
                            for column in key.columns:
                                if Col != "":
                                    Col += ", "
                                Col += column.name
                            html.write("                 <td>" + Col + "</td>\n")
                            html.write("                 <td>" + key.referencedTable.name + "</td>\n")
                            Col = ""
                            for column in key.referencedColumns: 
                                if Col != "":
                                    Col += ", "
                                Col += column.name
                            html.write("                 <td>" + Col + "</td>\n")
                            html.write("             </tr>\n")
                        html.write("             </tbody>\n")
                        html.write("         </table>\n")


                    if len(table.triggers) > 0:
                        html.write("     <div class=\"subtitle\">Triggers</div>\n")

                        html.write("         <table>\n")
                        html.write("             <thead>\n")
                        html.write("                 <tr>\n")
                        html.write("                     <th style=\"width:200px\">Trigger Name</th>\n")
                        html.write("                     <th style=\"width:100px\">Timing</th>\n")
                        html.write("                     <th style=\"width:100px\">Event</th>\n")
                        html.write("                     <th style=\"width:100%\">SQL Definition</th>\n")
                        html.write("                 </tr>\n")
                        html.write("             </thead>\n")
                        html.write("             <tbody>\n")
                    
                        for trigger in table.triggers:
                            html.write("                 <tr>\n")
                            html.write("                     <td>" + trigger.name + "</td>\n")
                            html.write("                     <td>" + trigger.timing + "</td>\n")
                            html.write("                     <td>" + trigger.event + "</td>\n")
                            html.write("                     <td style=\"color: #000000; font-weight: normal;\">" + Parse(trigger.sqlDefinition) + "</td>\n")
                            html.write("                 </tr>\n")

                        html.write("             </tbody>\n")
                        html.write("         </table>\n")

                    html.write("     </div>\n")

                html.write("<div id=\"" + schema.name + ".Views\"></div>")
                for view in sorted(schema.views, key=lambda view: view.name):
                    html.write(" <div class=\"title\">View " + view.name + "- <a href=\"#index\">Index</a>\n")
                    html.write("         <table>\n")
                    html.write("             <thead>\n")
                    html.write("                 <tr>\n")
                    html.write("                     <th>SQL Definition</th>\n")
                    html.write("                 </tr>\n")
                    html.write("             </thead>\n")
                    html.write("             <tbody>\n")
                    html.write("                <tr>")
                    html.write("                    <td style=\"color: #000000; font-weight: normal;\">" + Parse(view.sqlDefinition) + "</td>\n")
                    html.write("                </tr>")
                    html.write("             </tbody>\n")
                    html.write("         </table>\n")
                    html.write(" </div>")
                
                html.write("<div id=\"" + schema.name + ".Routines\"></div>")
                for routine in schema.routines:
                    RoutineDef = routine.routineType + " " + routine.name
                    if routine.returnDatatype != "":
                        RoutineDef += " RETURNS " + routine.returnDatatype
                    html.write(" <div class=\"title\">" + RoutineDef + "- <a href=\"#index\">Index</a>\n")
                    html.write("         <table>\n")
                    html.write("             <thead>\n")
                    html.write("                 <tr>\n")
                    html.write("                     <th>SQL Definition</th>\n")
                    html.write("                 </tr>\n")
                    html.write("             </thead>\n")
                    html.write("             <tbody>\n")
                    html.write("                <tr>")
                    html.write("                    <td style=\"color: #000000; font-weight: normal;\">" + Parse(routine.sqlDefinition) + "</td>\n")
                    html.write("                </tr>")
                    html.write("             </tbody>\n")
                    html.write("         </table>\n")
                    html.write(" </div>")

            html.write(" </body>\n")
            html.write("</html>")
            html.close()
    return 0

def WriteWord(word, prev, item):
    DataTypeList = ["BINARY", "BLOB", "LONGBLOB", "MEDIUMBLOB", "TINYBLOB", "VARBINARY", "DATE", "DATETIME", "TIME", "TIMESTAMP", "YEAR", "CURVE", "GEOMETRY", "GEOMETRYCOLLECTION", "LINE", "LINEARRING", "LINESTRING", "MULTICURVE", "MULTILINESTRING", "MULTIPOINT", "MULTIPOLYGON", "MULTISURFACE", "POINT", "POLYGON", "SURFACE", "BIGINT", "DECIMAL", "DOUBLE", "FLOAT", "INT", "MEDIUMINT", "SMALLINT", "TINYINT", "CHAR", "VARCHAR", "LONGTEXT", "MEDIUMTEXT", "TEXT", "TINYTEXT", "BIT", "ENUM", "BOOL", "BOOLEAN", "FIXED", "FLOAT4", "FLOAT8", "INT1", "INT2", "INT3", "INT4", "INT8", "INTEGER", "LONG", "MIDDLEINT", "NUMERIC", "DEC", "CHARACTER", "UNSIGNED"]
    FunctionList = ["OLD", "NEW", "ABS", "ACOS", "ADDDATE", "ADDTIME", "AES_DECRYPT", "AES_ENCRYPT", "ASCII", "ASIN", "ATAN2", "ATAN", "AVG", "BENCHMARK", "BIN", "BIT_AND", "BIT_COUNT", "BIT_LENGTH", "BIT_OR", "BIT_XOR", "CAST", "CEIL", "CEILING", "CHAR_LENGTH", "CHAR", "CHARACTER_LENGTH", "CHARSET", "COALESCE", "COERCIBILITY", "COLLATION", "COMPRESS", "CONCAT_WS", "CONCAT", "CONNECTION_ID", "CONV", "CONVERT_TZ", "CONVERT", "COS", "COT", "COUNT", "CRC32", "CURDATE", "CURRENT_DATE", "CURRENT_TIME", "CURRENT_TIMESTAMP", "CURRENT_USER", "CURTIME", "DATABASE", "DATE_ADD", "DATE_FORMAT", "DATE_SUB", "DATE", "DATEDIFF", "DAY", "DAYNAME", "DAYOFMONTH", "DAYOFWEEK", "DAYOFYEAR", "DECODE", "DEFAULT", "DEGREES", "DES_DECRYPT", "DES_ENCRYPT", "ELT", "ENCODE", "ENCRYPT", "EXP", "EXPORT_SET", "EXTRACT", "ExtractValue", "FIELD", "FIND_IN_SET", "FLOOR", "FORMAT", "FOUND_ROWS", "FROM_DAYS", "FROM_UNIXTIME", "GET_FORMAT", "GET_LOCK", "GREATEST", "GROUP_CONCAT", "HEX", "HOUR", "IF", "IFNULL", "IN", "INET_ATON", "INET_NTOA", "INSERT", "INSTR", "INTERVAL", "IS_FREE_LOCK", "IS_USED_LOCK", "ISNULL", "LAST_INSERT_ID", "LCASE", "LEAST", "LEFT", "LENGTH", "LN", "LOAD_FILE", "LOCALTIME", "LOCALTIMESTAMP", "LOCATE", "LOG10", "LOG2", "LOG", "LOWER", "LPAD", "LTRIM", "MAKE_SET", "MAKEDATE", "MAKETIME", "MASTER_POS_WAIT", "MAX", "MD5", "MICROSECOND", "MID", "MIN", "MINUTE", "MOD", "MONTH", "MONTHNAME", "NAME_CONST", "NOT IN", "NOW", "NULLIF", "OCT", "OCTET_LENGTH", "OLD_PASSWORD", "ORD", "PASSWORD", "PERIOD_ADD", "PERIOD_DIFF", "PI", "POSITION", "POW", "POWER", "PROCEDURE ANALYSE", "QUARTER", "QUOTE", "RADIANS", "RAND", "RELEASE_LOCK", "REPEAT", "REPLACE", "REVERSE", "RIGHT", "ROUND", "ROW_COUNT", "RPAD", "RTRIM", "SCHEMA", "SEC_TO_TIME", "SECOND", "SESSION_USER", "SHA1", "SHA", "SHA2", "SIGN", "SIN", "SLEEP", "SOUNDEX", "SPACE", "SQRT", "STD", "STDDEV_POP", "STDDEV_SAMP", "STDDEV", "STR_TO_DATE", "STRCMP", "SUBDATE", "SUBSTR", "SUBSTRING_INDEX", "SUBSTRING", "SUBTIME", "SUM", "SYSDATE", "SYSTEM_USER", "TAN", "TIME_FORMAT", "TIME_TO_SEC", "TIME", "TIMEDIFF", "TIMESTAMP", "TIMESTAMPADD", "TIMESTAMPDIFF", "TO_DAYS", "TO_SECONDS", "TRIM", "TRUNCATE", "UCASE", "UNCOMPRESS", "UNCOMPRESSED_LENGTH", "UNHEX", "UNIX_TIMESTAMP", "UpdateXML", "UPPER", "USER", "UTC_DATE", "UTC_TIME", "UTC_TIMESTAMP", "UUID_SHORT", "UUID", "VALUES", "VAR_POP", "VAR_SAMP", "VARIANCE", "VERSION", "WEEK", "WEEKDAY", "WEEKOFYEAR", "YEAR", "YEARWEEK"]
    KeyWordList = ["OPEN", "CLOSE", "ROW", "CHARSET", "RETURNS", "FUNCTION", "END", "DEFINER", "COMMENT", "BEGIN", "GENERAL", "IGNORE_SERVER_IDS", "MASTER_HEARTBEAT_PERIOD", "MAXVALUE", "RESIGNAL", "SIGNAL", "SLOW ", "ACCESSIBLE", "ADD", "ALL", "ALTER", "ANALYZE", "AND", "AS", "ASC", "ASENSITIVE", "BEFORE", "BETWEEN", "BIGINT", "BINARY", "BLOB", "BOTH", "BY", "CALL", "CASCADE", "CASE", "CHANGE", "CHAR", "CHARACTER", "CHECK", "COLLATE", "COLUMN", "CONDITION", "CONSTRAINT", "CONTINUE", "CONVERT", "CREATE", "CROSS", "CURRENT_DATE", "CURRENT_TIME", "CURRENT_TIMESTAMP", "CURRENT_USER", "CURSOR", "DATABASE", "DATABASES", "DAY_HOUR", "DAY_MICROSECOND", "DAY_MINUTE", "DAY_SECOND", "DEC", "DECIMAL", "DECLARE", "DEFAULT", "DELAYED", "DELETE", "DESC", "DESCRIBE", "DETERMINISTIC", "DISTINCT", "DISTINCTROW", "DIV", "DOUBLE", "DROP", "DUAL", "EACH", "ELSE", "ELSEIF", "ENCLOSED", "ESCAPED", "EXISTS", "EXIT", "EXPLAIN", "FALSE", "FETCH", "FLOAT", "FLOAT4", "FLOAT8", "FOR", "FORCE", "FOREIGN", "FROM", "FULLTEXT", "GRANT", "GROUP", "HAVING", "HIGH_PRIORITY", "HOUR_MICROSECOND", "HOUR_MINUTE", "HOUR_SECOND", "IF", "IGNORE", "IN", "INDEX", "INFILE", "INNER", "INOUT", "INSENSITIVE", "INSERT", "INT", "INT1", "INT2", "INT3", "INT4", "INT8", "INTEGER", "INTERVAL", "INTO", "IS", "ITERATE", "JOIN", "KEY", "KEYS", "KILL", "LEADING", "LEAVE", "LEFT", "LIKE", "LIMIT", "LINEAR", "LINES", "LOAD", "LOCALTIME", "LOCALTIMESTAMP", "LOCK", "LONG", "LONGBLOB", "LONGTEXT", "LOOP", "LOW_PRIORITY", "MASTER_SSL_VERIFY_SERVER_CERT", "MATCH", "MAXVALUE", "MEDIUMBLOB", "MEDIUMINT", "MEDIUMTEXT", "MIDDLEINT", "MINUTE_MICROSECOND", "MINUTE_SECOND", "MOD", "MODIFIES", "NATURAL", "NOT", "NO_WRITE_TO_BINLOG", "NULL", "NUMERIC", "ON", "OPTIMIZE", "OPTION", "OPTIONALLY", "OR", "ORDER", "OUT", "OUTER", "OUTFILE", "PRECISION", "PRIMARY", "PROCEDURE", "PURGE", "RANGE", "READ", "READS", "READ_WRITE", "REAL", "REFERENCES", "REGEXP", "RELEASE", "RENAME", "REPEAT", "REPLACE", "REQUIRE", "RESIGNAL", "RESTRICT", "RETURN", "REVOKE", "RIGHT", "RLIKE", "SCHEMA", "SCHEMAS", "SECOND_MICROSECOND", "SELECT", "SENSITIVE", "SEPARATOR", "SET", "SHOW", "SIGNAL", "SMALLINT", "SPATIAL", "SPECIFIC", "SQL", "SQLEXCEPTION", "SQLSTATE", "SQLWARNING", "SQL_BIG_RESULT", "SQL_CALC_FOUND_ROWS", "SQL_SMALL_RESULT", "SSL", "STARTING", "STRAIGHT_JOIN", "TABLE", "TERMINATED", "THEN", "TINYBLOB", "TINYINT", "TINYTEXT", "TO", "TRAILING", "TRIGGER", "TRUE", "UNDO", "UNION", "UNIQUE", "UNLOCK", "UNSIGNED", "UPDATE", "USAGE", "USE", "USING", "UTC_DATE", "UTC_TIME", "UTC_TIMESTAMP", "VALUES", "VARBINARY", "VARCHAR", "VARCHARACTER", "VARYING", "WHEN", "WHERE", "WHILE", "WITH", "WRITE", "XOR", "YEAR_MONTH", "ZEROFILL"]

    if str.upper(word) in DataTypeList:
        return "<span style=\"font-weight: bold;\">" + word + "</span>"
    elif str.upper(word) in KeyWordList:
        return "<span style=\"color: blue;\">" + word + "</span>"
    elif str.upper(word) in FunctionList:
        return "<span style=\"color: magenta;\">" + word + "</span>"
    elif word[:2] == "@@":
        return "<span style=\"color: green;font-weight: bold\">" + word + "</span>"
    elif word[:1] == "@" and prev != "`" and item != "`" :
        return "<span style=\"color: green;\">" + word + "</span>"    
    else:
        try:
            int(word)
            return "<span style=\"color: brown;\">" + word + "</span>"
        except ValueError:
            return word

def Parse(SQLDefinition):
    LSQLDefinition = list(SQLDefinition)
    FinalSTR = ""
    word = ""
    prev = ""
    prevprev = ""
    IsInStr = False
    IsInInlineComment = False
    IsInMultilineComment = False
    InStrFrom = 0
    StrDelimiter = ""
    for index, item in enumerate(list(SQLDefinition)):        
        if IsInMultilineComment:
            if item == "<":
                word += "&lt;"
            elif item == ">":
                word += "&gt;" 
            elif item == " ":
                word += "&nbsp;"
            elif item == "\t":
                word += "&nbsp;&nbsp;"
            elif item == "\n":
                word += "<br />"
            else:
                word += item
            if item == "/" and prev == "*":
                FinalSTR += "<span style=\"color: teal;\">" + word + "</span>"
                IsInMultilineComment = False
                word = ""
        elif IsInInlineComment:
            word += item  
            if item == "\n":
                FinalSTR += "<span style=\"color: teal;\">" + word + "</span><br />"
                IsInInlineComment = False
                word = ""          
        elif IsInStr:
            word += item
            if item == StrDelimiter and ((prev != StrDelimiter and (prev != "\\" or prevprev == "\\")) or index-1 == InStrFrom):
                FinalSTR += "<span style=\"color: red;\">" + word + "</span>"
                IsInStr = False
                word = ""
        else:
            if str.upper(item) in ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "_", "@"]:
                word += item
            else:
                if word != "":
                    FinalSTR += WriteWord(word, prev, item)
                    word = ""
                if item == "-" and LSQLDefinition[index+1] == "-" and LSQLDefinition[index+2] == " ":
                    IsInInlineComment = True
                    word = item
                elif item == "/" and LSQLDefinition[index+1] == "*":
                    IsInMultilineComment = True
                    word = item
                elif item in ["&", "=", ":", "~", "|", "^", "/", "<", ">", "-", "%", "!", "+", "*"] and prev != "`" and LSQLDefinition[index+1] != "`" :
                    FinalSTR += "<span style=\"color: grey;\">" + item.replace("<", "&lt;").replace(">", "&gt;")  + "</span>"
                elif item == " ":
                    FinalSTR += "&nbsp;"
                elif item == "\t":
                    FinalSTR += "&nbsp;&nbsp;"
                elif item == "\n":
                    FinalSTR += "<br />"
                elif item in ["\"", "'"]:
                    StrDelimiter = item
                    word = item
                    IsInStr = True
                    InStrFrom = index
                else:
                    FinalSTR += item
        prevprev = prev
        prev = item
    FinalSTR += WriteWord(word, prev, item)
    return FinalSTR
